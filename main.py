import requests
from datetime import datetime, timezone
from bs4 import BeautifulSoup as bs
import json

gdqURL = 'https://gamesdonequick.com/schedule/43'
scheduleData = {}

req = requests.get(gdqURL)
if (req.status_code == 200):
    print('200 received from gdq site, parsing code...')
    soup = bs(req.content, 'html.parser')
    runTable = soup.find(id="runTable")
    tableBody = runTable.tbody
    for row in tableBody.find_all(class_="second-row"):
        runnerData = row.find_previous_sibling("tr")
        if (runnerData is not None and len(runnerData.select("td:nth-of-type(4)")) > 0):
            runLength = row.select("td:nth-of-type(1)")[0].contents[2][1:][:-1]
            try:
                runType, console = row.select("td:nth-of-type(2)")[0].contents[0].split(" — ")
            except ValueError:
                runType, console = ('any%', 'GDQ')
            host = row.select("td:nth-of-type(3)")[0].contents[1][1:]
            startTime = datetime.strptime(runnerData.select("td:nth-of-type(1)")[0].contents[0], '%Y-%m-%dT%H:%M:%SZ')
            startTime = startTime.replace(tzinfo=timezone.utc)
            gTcontent = runnerData.select("td:nth-of-type(2)")[0].findAll(text=True)
            if len(gTcontent) > 1:
                gameTitle = gTcontent[1].strip()
            else:
                gameTitle = gTcontent[0].strip()
            runner = runnerData.select("td:nth-of-type(3)")[0].contents[0]
            try:
                setupLength = runnerData.select("td:nth-of-type(4)")[0].contents[2][1:][:-1]
            except IndexError:
                setupLength = '0:00:00'
            scheduleData[int(startTime.timestamp())] = {
                "length": runLength,
                "type": runType,
                "runner": runner,
                "host": host,
                "console": console,
                "title": gameTitle,
                "setup": setupLength
            }
    file = open("/var/www/rb-node/public/gdqsched.json", "w")
    file.write(json.dumps(scheduleData))
    file.close()
    print('JSON written, finished')
else:
    print('error received! check URL to ensure it resolves without issue')
